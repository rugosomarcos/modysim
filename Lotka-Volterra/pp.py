import numpy as np

import matplotlib.pyplot as plt
from scipy.integrate import odeint

np.seterr(divide='ignore', invalid='ignore')

# Parámetros
r1 = 0.1
p = 0.02
r2 = 0.3
a = 0.01
r = 0.001

def df_dt(x, t, r1, p, r2, a):
    dx = r1 * x[0] - p * x[0] * x[1]
    dy = -r2 * x[1] + a * x[0] * x[1]
    return np.array([dx, dy])


def df_dt_logistic(x, t, r1, p, r2, a, r):
    dx = r1 * x[0] - r * x[0] ** 2 - p * x[0] * x[1]
    dy = - r2 * x[1] + a * x[0] * x[1]
    return np.array([dx, dy])


# Condiciones iniciales

x0 = 500    # Presas
y0 = 10  # Depredadores

conds_iniciales = np.array([x0, y0])

# Condiciones para integración
tf = 500
n = 2000
t = np.linspace(0, tf, n)

solution = odeint(df_dt, conds_iniciales, t, args=(r1, p, r2, a))

solution_logistic = odeint(df_dt_logistic, conds_iniciales, t, args=(r1, p, r2, a, r))

n_max = np.max(solution) * 1.10

x_max = np.max(solution_logistic[:, 0]) * 1.05
y_max = np.max(solution_logistic[:, 1]) * 1.05

x = np.linspace(0, x_max, 25)
y = np.linspace(0, y_max, 25)

xx, yy = np.meshgrid(x, y)
uu, vv = df_dt_logistic((xx, yy), 0, r1, p, r2, a, r)
norm = np.sqrt(uu**2 + vv**2)
uu = uu / norm
vv = vv / norm

plt.clf()
plt.plot(t, solution_logistic[:, 0], 'r-', label='presa')
plt.plot(t, solution_logistic[:, 1], 'b-', label='depredador')
plt.legend()
plt.xlabel('tiempo (semanas)', fontsize=12)
plt.ylabel('Escenario de Poblaciones', fontsize=12)

plt.savefig('presa-depredador.png')

plt.clf()
plt.quiver(xx, yy, uu, vv, norm, cmap=plt.cm.gray)
plt.plot(solution_logistic[:, 0], solution_logistic[:, 1], lw=2, alpha=0.8)
plt.xlabel('presa x', fontsize=12)
plt.ylabel('depredador y', fontsize=12)
plt.savefig('diagrama-de-fase.png')

def c(x, r1, p, r2, a):
    return r1 * np.log(x[1]) - p * x[1] + r2 * np.log(x[0]) - a * x[0]

x = np.linspace(0, x_max, 100)
y = np.linspace(0, y_max, 100)
xx, yy = np.meshgrid(x, y)
constant = c((xx, yy), r1, p, r2, a)

plt.figure('distintas_soluciones', figsize=(8,5))
plt.contour(xx, yy, constant, 50, cmap=plt.cm.Blues)
plt.xlabel('presas')
plt.ylabel('depredadores')
plt.savefig('punto-de-estabilidad.png')

