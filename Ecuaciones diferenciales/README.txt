ECUACION DIFERENCIAL CUADRATICA

Ecuación a resolver:

df(t)/dt = αf(t) + βf(t)²

Encontramos:

- Solucion general:

f(t) = α*exp(α*(C1 + t))/(β*(1 - exp(α*(C1 + t)))))

- Condicion inicial:
Eq(alpha*exp(C1*alpha)/(beta*(1 - exp(C1*alpha))), p_0)
log(beta*p_0/(alpha + beta*p_0))/alpha

- Su solucion particular:

f(t) = α*exp(α*(p_0 + t))/(β*(1 - exp(α*(p_0 + t)))))

- Solucion Particular:

alpha*exp(alpha*(t + log(beta*p_0/(alpha + beta*p_0))/alpha))/(beta*(1 - exp(alpha*(t + log(beta*p_0/(alpha + beta*p_0))/alpha))))

- Solución Simplificada

alpha*p_0*exp(alpha*t)/(alpha - beta*p_0*exp(alpha*t) + beta*p_0)


