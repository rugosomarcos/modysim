import numpy as np
import matplotlib.pyplot as plt
import scipy.stats as ss
import itertools as it
import random


datos = [33, 18, 27, 36, 25, 24, 30, 23, 29, 38, 23, 22, 19, 21, 18, 28, 12, 20, 22, 22, 22, 34, 32, 15, 14, 16, 32,
         17, 22, 37, 11, 22, 37, 15, 29, 11, 36, 24, 18, 19, 23, 12, 21, 7, 30, 6, 26, 31, 24, 31, 25, 21, 25, 21, 24,
         30, 29, 18, 19, 16, 17, 20, 24, 18, 19, 43, 9, 37, 32, 26, 25, 18, 16, 14, 23, 37, 31, 34, 21, 22, 26, 16, 24,
         25, 32, 11, 30, 30, 17, 18, 36, 14, 31, 24, 35, 12, 19, 17, 21, 28, 14, 22, 39, 27, 18, 33, 39, 30, 32, 26, 32,
         19, 35, 26, 20, 23, 23, 23, 23, 11, 20, 15, 19, 33, 23, 40, 25, 18, 16, 20, 23, 20, 31, 23, 20, 38, 30, 19, 15,
         6, 23, 23, 21, 17, 27, 19, 23, 44, 19, 23, 20, 24, 22, 18, 23, 21, 16, 10, 23, 26, 27, 29, 21, 13, 32, 22, 27,
         28, 35, 24, 16, 14, 25, 32, 22, 32, 24, 33, 32, 32, 25, 24, 28, 23, 16, 27, 27, 10, 32, 13, 29, 27, 22, 17, 25,
         33, 46, 17, 35, 17, 20, 22, 30, 31, 27, 21, 32, 18, 19, 25, 26, 20, 33, 19, 30, 11, 34, 34, 32, 24, 30, 26, 28,
         29, 19, 21, 21, 30, 26, 25, 18, 22, 21, 37, 24, 25, 35, 18, 16, 29, 25, 20, 26, 28, 32, 16, 15, 42, 29]

array = np.array(datos)

# Media
media = np.mean(array)

# Varianza
var = np.var(array)

# Desviación estandar
dv = np.std(array)

# Máximos y mínimos
min = np.min(array)
max = np.max(array)

# Intervalos de 6 a 46
x = np.arange(min, max+1)

# Distribución normal
dn = ss.norm.pdf(x, loc=media, scale=dv)
plt.plot(x, dn)
plt.title('Distribución Normal')
plt.show()

# Distribución acumulada
da = list(it.accumulate(dn))

plt.plot(x, da)
plt.title('Distribución Acumulada')
plt.show()


# Montecarlo
r = []
for i in range(0,100):
    r.append(random.uniform(0, 1))

plt.plot(r)
plt.title('Randoms')
plt.show()

dac = da.copy()


array_dg = []


for i in range(0,99):
    for j in range(1,40):
        if r[i] <= da[0]:
             
            array_dg.append(x[j])  
                
        elif da[j-1] < r[i] < da[j]:
           
            array_dg.append(x[j])       

print("Predicción de visitas por día:")
print(array_dg)
     
plt.plot(dac)
plt.title('Visitas')
plt.show()


# Media
media2 = np.mean(array_dg)

print("Media de los datos generados:")
print(media2)
# Varianza
print("Varianza de los datos generados:")
var2 = np.var(array_dg)
print(var2)
# Desviación estandar
print("Desvio estándar de los datos generados:")
dv2 = np.std(array_dg)
print(dv2)


